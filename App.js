/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
  StatusBar,
  Button
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import CarInfo from './src/components/atom/CarInfo';

const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView>
          <Image 
            source={{uri: "https://static.designboom.com/wp-content/uploads/2019/09/new-2020-ferrari-f8-spider-designboom-4.jpg"}} 
            style={{height: 400, width: 400}} />          
          
      
          <Text>
          ferrari unveils the f8 spider, a new generation drop-top sports car equipped with the most successful mid-rear-mounted v8 in history. the f8 spider replaces the 488 spider, boasting an engine that delivers an extra 50 cv. the new ferrari is also 20 kg lighter than its predecessor, weighing just 20 kg more than the more extreme 488 pista spider.
          the 3902 CC unit unleashes 720 cv at 8,000 rpm and also has an impressive specific power output of 185 cv/l. its maximum torque is now higher at all engine speeds, peaking at 770 nm at 3,250 rpm. the v8 is coupled to a 7-speed dual-clutch transmission and has a redline of 8,000 rpm. ferrari claims the setup is able to rocket the car from 0-62 mph in 2.9 seconds, and 0-124 mph in just 8.2 seconds. top speed is 211 mph.
          the 2020 f8 spider is significantly more aerodynamically efficient and features the new 6.1 version of the side slip angle control system. ferrari has moved the air intakes from the flanks to the rear where they are located on either side of the blown spoiler and are directly connected to the intake plenums. this drastically reduces losses and ensures greater air flow to the engine, thereby increasing the power. the air flow also benefits from increased dynamic pressure created by the shape of the rear spoiler.
          ferrari unveils the f8 spider, a new generation drop-top sports car equipped with the most successful mid-rear-mounted v8 in history. the f8 spider replaces the 488 spider, boasting an engine that delivers an extra 50 cv. the new ferrari is also 20 kg lighter than its predecessor, weighing just 20 kg more than the more extreme 488 pista spider.
          the 3902 CC unit unleashes 720 cv at 8,000 rpm and also has an impressive specific power output of 185 cv/l. its maximum torque is now higher at all engine speeds, peaking at 770 nm at 3,250 rpm. the v8 is coupled to a 7-speed dual-clutch transmission and has a redline of 8,000 rpm. ferrari claims the setup is able to rocket the car from 0-62 mph in 2.9 seconds, and 0-124 mph in just 8.2 seconds. top speed is 211 mph.
          the 2020 f8 spider is significantly more aerodynamically efficient and features the new 6.1 version of the side slip angle control system. ferrari has moved the air intakes from the flanks to the rear where they are located on either side of the blown spoiler and are directly connected to the intake plenums. this drastically reduces losses and ensures greater air flow to the engine, thereby increasing the power. the air flow also benefits from increased dynamic pressure created by the shape of the rear spoiler.
          </Text>
         <CarInfo />

          <Button
  title="Learn More"
  color="#841584"/>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
